export default {
    day : {
        id: 'day',
        name: 'Day Mode',
        fontColor: '#000000',
        backgroundColor: '#ffffff',
        grid : {
            borderColor: '#f2f4f5',
            fontColor: '#96a2aa',
            preview : {
                color: '#e2f3fb',
                selectorColor: '#c5e0f3',
            }
        },
        gui: {
            borderColor: '#f2f4f5',
            fontColor: '#108be3'
        }
    },
    night : {
        id: 'night',
        name: 'Night Mode',
        fontColor: '#e8ecee',
        backgroundColor: '#242f3e',
        grid : {
            borderColor: '#3b4a5a',
            fontColor: '#546778',
            preview : {
                color: '#1f2a38',
                selectorColor: '#40566b',
            }
        },
        gui: {
            borderColor: '#3b4a5a',
            fontColor: '#36a8f1'
        }
    }
}
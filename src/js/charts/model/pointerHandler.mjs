import {Rect} from "../components/rect.mjs";
import {Point} from "../components/point.mjs";
import PointerEventManager from "../services/pointerEventManager.mjs";

class PointerHandler {
    // container = null;
    rect = new Rect();
    isDragging = false;
    dragStart = new Point();
    events = new Map();

    constructor() {
        this.initEvents();
    }

    initEvents() {
        //mouse
        this.events.set('mousedown', this.onMouseDown.bind(this));
        this.events.set('mouseup', this.onMouseUp.bind(this));
        this.events.set('mousemove', this.onMouseMove.bind(this));
        //touch
        // this.events.set('touchstart', this.onMouseDown.bind(this));
        // this.events.set('touchend', this.onMouseUp.bind(this));
        // this.events.set('touchmove', this.onMouseMove.bind(this));
    }

    onMouseDown(point) {
        PointerEventManager.dragHandler = this;
        this.isDragging = true;
        this.dragStart.x = point.x;
        this.dragStart.y = point.y;
// console.log('onMouseDown', point);
    }

    onMouseUp(point) {
        this.isDragging = false;
// console.log('onMouseUp', point);
    }

    onMouseMove(point) {
        const event = this.isDragging ? 'drag' : 'hover';
        if(!this.events.has(event)) return;

        if(this.isDragging) {
            this.onMouseDrag(point);
        } else {
            this.onMouseHover(point);
        }
    }

    onMouseDrag(point) {
        const event = 'drag';
        if(!this.events.has(event)) return;
        const result = this.events.get(event)(point, this.dragStart);

        if(!result) return;
        this.dragStart.x = point.x;
        this.dragStart.y = point.y;
    }
    onMouseHover(point) {
        const event = 'hover';
        if(!this.events.has(event)) return;
        this.events.get(event)(point);
    }
}

export { PointerHandler };
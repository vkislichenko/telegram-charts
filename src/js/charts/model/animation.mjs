class Animation {
    options = {
        isActive: false,
        fpsLimit: null,
        frameAmount: 10,
        frameLoop: false,
        animate: null,
    };

    frameCurrent = 0;
    lastTickTimestamp = null;
    data = new Map();

    constructor(options) {
        this.initOptions(options)
    }

    initOptions(options) {
        this.options = Object.assign({}, this.options, options || {});
    }

    get active() {
        return this.options.isActive;
    }
    set active(value) {
        this.options.isActive = value;
    }

    get canTick() {
// console.log('canTick', this.options.isActive, this.options.animate, this.frameCurrent+1 <= this.options.frameAmount, this.options.frameLoop);
        return (
            this.options.isActive && this.options.animate &&
            (this.frameCurrent+1 <= this.options.frameAmount || this.options.frameLoop)
        );
    }

    reset() {
        this.frameCurrent = 0;
        return this;
    }
    activate() {
        this.reset();
        this.active = true;
        return this;
    }

    tick(timestamp) {
        if(!this.canTick) return;

        if(!this.lastTickTimestamp) this.lastTickTimestamp = timestamp;
        const delta = timestamp - this.lastTickTimestamp;

        if(this.options.fpsLimit) {
            const deltaLimit = 1000/this.options.fpsLimit;
            if(delta && deltaLimit >= delta) {
                return;
            }
        }

        //increment current frame, or loop it back to first one
        if(this.options.frameLoop && this.frameCurrent+1 > this.options.frameAmount) {
            this.frameCurrent = 0;
        }
        this.frameCurrent++;
// console.log('tick', delta, this.frameCurrent);
        this.options.animate(this);

        this.lastTickTimestamp = timestamp;
    }

    animate() {

    }
}

export { Animation };
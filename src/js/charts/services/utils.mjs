class Utils {
    debounce(fn, time) {
        let timeout;
        return function() {
            const functionCall = () => fn.apply(this, arguments);
            clearTimeout(timeout);
            timeout = setTimeout(functionCall, time);
        }
    }

    /**
     * Retrieve a evenly distributed fixed number of elements from an array
     *
     * @param   {Array} items - The array to operate on
     * @param   {Number} n - The number of elements to extract
     * @returns {Array}
     */
    distributedCopy(items, n) {
        let elements = [];
        let totalItems = items.length;
        let interval = Math.ceil(totalItems/n);

        for (let i = 0; i < items.length; i++) {
            if(i % interval !== 0) continue;
            elements.push(items[i]);
        }

        return elements;
    }

    uuid() {
        let d = new Date().getTime();
        if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
            d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    clone(orig) {
        return Object.assign( Object.create( Object.getPrototypeOf(orig)), orig);
    }

    generateDataSet(size = 1000000) {
        let result = {"columns":[["x"],["y0"],["y1"]],"types":{"y0":"line","y1":"line","x":"x"},"names":{"y0":"#0","y1":"#1"},"colors":{"y0":"#3DC23F","y1":"#F34C44"}};
        for (let i = 1; i <= size; i++) {
            result.columns[0][i] = i * 24*60*60*1000;
            result.columns[1][i] = (Math.random() * 20 + (result.columns[1][i-1] - 10 || 0) + 0.5) << 0;
            result.columns[2][i] = (Math.random() * 20 + (result.columns[2][i-1] - 10 || 0) + 0.5) << 0;
        }
        return result;
    }
}

export default new Utils;
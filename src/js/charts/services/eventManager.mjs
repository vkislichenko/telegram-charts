class EventManagerService {
    constructor (eventMap = new Map ()) {
        this.eventMap = eventMap;
    }
    addEventListener (event, handler) {
        if (!this.eventMap.has(event)) {
            this.eventMap.set(event, [handler]);
            return;
        }
        this.eventMap.set(event, this.eventMap.get(event).concat([handler]));
    }
    dispatchEvent (event) {
        if (!this.eventMap.has(event)) return;
        this.eventMap.get(event).forEach(a => a());
    }
}

export default new EventManagerService();
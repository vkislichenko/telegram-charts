import {Point} from "../components/point.mjs";

class PointerEventManagerService {
    dom = {
        canvas : null,
    };

    hitMap = new Map();

    events = [
        // 'click',
        'mousedown', 'mouseup', 'mousemove',
        // 'touchstart', 'touchend', 'touchmove'
    ];

    dragHandler = null;
    dragProcessEvents = ['mouseup', 'mousemove']; //events that will look for previously saved drag handler
    dragCancelEvents = ['mousedown', 'mouseup']; //events that will clear previously saved drag handler

    initCanvasEvents() {
        if(!this.dom.canvas) return;

        //init mouse events
        this.events.forEach((event, index) => {
            this.initEvent(event);
        });
        //init touch events, kinda
        this.initFakeTouchEvents();
    }

    initEvent(eventName) {
        this.dom.canvas.addEventListener(eventName, (e) => {
            const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            const point = new Point(e.clientX, e.clientY + scrollTop);
            let handler = null;
            if(this.dragProcessEvents.indexOf(eventName) !== -1) handler = this.dragHandler;
            if(this.dragCancelEvents.indexOf(eventName) !== -1) this.dragHandler = null;
            if(!handler) handler = this.getIntersection(point, eventName);
            if(!handler) return;
            handler.events.get(eventName)(point);
        });
    }

    initFakeTouchEvents() {
        const canvas = this.dom.canvas;
        const mapper = {
            'touchstart': 'mousedown',
            'touchend': 'mouseup',
            'touchcancel': 'mouseup',
            'touchmove': 'mousemove',
        };
        for(let event in mapper) {
            let mappedEvent = mapper[event];
            canvas.addEventListener(event, (e) => {
                const touch = e.touches[0];
                const mouseEvent = new MouseEvent(mappedEvent, {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });
                canvas.dispatchEvent(mouseEvent);
            }, false);
        }
    }

    getIntersection(point, event) {
        let result = null;
        this.hitMap.forEach((handler, name) => {
            if(!handler.events.has(event)) return;
            if(result) return;
            if(!handler.rect.contains(point)) return;
            result = handler;
        });

        return result;
    }
}

export default new PointerEventManagerService();
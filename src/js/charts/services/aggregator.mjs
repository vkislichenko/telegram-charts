import state from "./state.mjs";
import * as Components from "../components/index.mjs";
import EventManager from "../services/eventManager.mjs";
import {Point} from "../components/point.mjs";

class Aggregator {
    data = {};

    constructor () {

    }

    importChartData(data) {
//console.log(data);
        state.clearCharts();

        data.forEach((item, key) => {
            const chart = new Components.Chart();
            chart.import(item);
            state.addChart(chart);

            //activate first char on data load
            if(!state.activeChart) {
                chart.activate();
            }
            // if(key === 0) chart.isActive = true;
            // if(key === 4) chart.isActive = true;
        });
        EventManager.dispatchEvent('afterChartDataImport');
    }

    clearVisibleData() {
        this.data.visibleBounds = null;
        this.data.visibleBoundsAxisX = null;
    }

    clearData() {
        this.data.lines = null;
        this.data.pointsAxisX = null;
        this.data.pointsAxisY = null;
        this.data.bounds = null;
        this.data.rulerPointsAxisY = null;
        this.data.visibleBounds = null;
        this.data.visibleBoundsAxisX = null;
    }

    //TODO: clear this.data.lines when charts/lines are selected/deselected
    get lines() {
        if(this.data.lines) return this.data.lines;

        const result = [];
        if(!state.charts.size || !state.activeChart) return result;

        // state.charts.forEach((chart, chartIndex) => {
        //     if(!chart.isActive) return;
        //     chart.lines.forEach((line, lineIndex) => {
        //         result.push(line);
        //     });
        // });

        state.activeLines.forEach((line, key) => {
            result.push(line);
        });

        this.data.lines = result;
        return this.data.lines;
    }

    get visibleLines() {
        const lines = this.lines;
        if(!lines.length) return [];

        return lines.filter((line) => {
            return line.isActive;
        });
    }

    get pointsAxisX() {
        if(this.data.pointsAxisX) return this.data.pointsAxisX;

        //collect all points
        let result = [];
        if(!this.lines.length) return result;

        this.lines.forEach((line, lineIndex) => {
            line.points.forEach((point, pointIndex) => {
                result.push(point.x);
            });
        });

        //filter out non unique points
        result = result.filter((elem, pos, arr) => {
            return arr.indexOf(elem) == pos;
        });
        result.sort((a,b) => {
            return a - b;
        });

        this.data.pointsAxisX = result;
        return this.data.pointsAxisX;
    }

    get pointsAxisY() {
        if(this.data.pointsAxisY) return this.data.pointsAxisY;

        //collect all points
        let result = [];
        if(!this.lines.length) return result;

        this.lines.forEach((line, lineIndex) => {
            line.points.forEach((point, pointIndex) => {
                result.push(point.y);
            });
        });

        //filter out non unique points
        result = result.filter((elem, pos, arr) => {
            return arr.indexOf(elem) == pos;
        });
        result.sort((a,b) => {
            return a - b;
        });

        this.data.pointsAxisY = result;
        return this.data.pointsAxisY;
    }

    get bounds() {
        if(this.data.bounds) return this.data.bounds;

        this.data.bounds = {
            min: new Point(),
            max: new Point()
        };

        this.lines.forEach((line, lineIndex) => {
            if(lineIndex === 0 || this.data.bounds.min.x > line.bounds.min.x) this.data.bounds.min.x = line.bounds.min.x;
            if(lineIndex === 0 || this.data.bounds.min.y > line.bounds.min.y) this.data.bounds.min.y = line.bounds.min.y;
            if(lineIndex === 0 || this.data.bounds.max.x < line.bounds.max.x) this.data.bounds.max.x = line.bounds.max.x;
            if(lineIndex === 0 || this.data.bounds.max.y < line.bounds.max.y) this.data.bounds.max.y = line.bounds.max.y;
        });

        return this.data.bounds;
    }

    get visibleBoundsAxisX() {
        if(this.data.visibleBoundsAxisX) return this.data.visibleBoundsAxisX;

        const points = this.pointsAxisX;
        if(!points.length) return [];

        let minIndex = ((points.length-1) * state.selection.min) >> 0;
        let maxIndex = ((points.length-1) * state.selection.max) >> 0;
        if(maxIndex < (points.length-1)) maxIndex++;
        const minX = points[minIndex];
        const maxX = points[maxIndex];

        this.data.visibleBoundsAxisX = {
            min: minX,
            max: maxX,
        };

        return this.data.visibleBoundsAxisX;
    }

    get visibleBounds() {
        if(this.data.visibleBounds) return this.data.visibleBounds;

        this.data.visibleBounds = {
            min: new Point(),
            max: new Point()
        };

        // this.data.visibleBounds.min.y = 0;
        this.lines.forEach((line, lineIndex) => {
            const points = line.visiblePoints; //just get it for recalc
            if(lineIndex === 0 || this.data.visibleBounds.min.x > line.visibleBounds.min.x) this.data.visibleBounds.min.x = line.visibleBounds.min.x;
            if(lineIndex === 0 || this.data.visibleBounds.min.y > line.visibleBounds.min.y) this.data.visibleBounds.min.y = line.visibleBounds.min.y;
            if(lineIndex === 0 || this.data.visibleBounds.max.x < line.visibleBounds.max.x) this.data.visibleBounds.max.x = line.visibleBounds.max.x;
            if(lineIndex === 0 || this.data.visibleBounds.max.y < line.visibleBounds.max.y) this.data.visibleBounds.max.y = line.visibleBounds.max.y;
        });

        return this.data.visibleBounds;
    }

    get visiblePointsAxisX() {
        const bounds = this.visibleBoundsAxisX;

        //filter out invisible points
        const points = this.pointsAxisX;
        const result = points.filter((elem, pos, arr) => {
            return (elem >= bounds.min && elem <= bounds.max);
        });

        return result;
    }

    get visiblePointsAxisY() {
        const bounds = this.visibleBounds;

        //filter out invisible points
        const points = this.pointsAxisY;
        const result = points.filter((elem, pos, arr) => {
            return (elem >= bounds.min.y && elem <= bounds.max.y);
        });

        return result;
    }

    //TODO: refactoring to work with visible bounds, and generate marks on the fly
    get rulerPointsAxisY() {
        if(this.data.rulerPointsAxisY) return this.data.rulerPointsAxisY;

        const result = [];
// return result;
        const bounds = this.bounds;
        const pointsAxisY = this.pointsAxisY;

        const iteration = ((bounds.max.y - bounds.min.y) / pointsAxisY.length) >> 0;
        // const iteration = ((bounds.max.y - bounds.min.y) / 100) >> 0;

        result.push(bounds.min.y);
        for(let i = bounds.min.y; i < bounds.max.y; i+=iteration) {
            // if(i % iteration !== 0) continue;
            result.push(i)
        }

        this.data.rulerPointsAxisY = result;

        return this.data.rulerPointsAxisY;
    }

    getClosestPointAxisX(point) {
        const visiblePointsAxisX = this.visiblePointsAxisX;
        if(!visiblePointsAxisX.length) return null;
        //closest point according to math
        let result = visiblePointsAxisX.reduce((prev, curr) => {
            return (Math.abs(curr - point) < Math.abs(prev - point) ? curr : prev);
        });
        //and now make it look right :)
        let resultIndex = visiblePointsAxisX.indexOf(result);
        if(resultIndex < visiblePointsAxisX.length - 1) {
            let nextResult = visiblePointsAxisX[resultIndex + 1];
            if(point > result && (nextResult - point > point - result)) {
                result = nextResult;
            }
        }
        return result;
    }

    getInfoByAxisPointX(pointX) {
        const result = {
            x: pointX,
            y: [],
        };

        this.visibleLines.forEach((line, index) => {
            const linePoint = line.points.find((point) => {
                return point.x === pointX;
            });
            if(!linePoint) return;

            result.y.push({
                value: linePoint.y,
                name: line.name,
                color: line.color,
            });
        });

        return result;
    }

    get theme() {
        return state.activeTheme;
    }
    set theme(theme) {
        state.activeTheme = theme;
    }
    get themes() {
        return state.data.themes;
    }
    get nextTheme() {
        let activeThemeId = (this.theme) ? this.theme.id : null;
        let themeKeys = Object.keys(this.themes);
        let activeThemeKey = (activeThemeId) ? themeKeys.indexOf(activeThemeId) : -1;
        let nexThemeKey = activeThemeKey + 1;
        if(nexThemeKey > themeKeys.length - 1) nexThemeKey = 0;
        const result = this.themes[themeKeys[nexThemeKey]];
        return result;
    }
}

export default new Aggregator();
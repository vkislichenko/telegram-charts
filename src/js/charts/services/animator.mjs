import { Animation } from "../model/animation.mjs";

class Animator {

    animations = new Map();

    addAnimation (name, animationOptions) {
        const animation = new Animation(animationOptions)
        this.animations.set(name, animation);
        return animation;
    }
    getAnimation(name) {
        return this.animations.get(name) || null;
    }

    animate(timestamp) {
        /** @var BasicAnimation animation */
        this.animations.forEach((animation, key) => {
            animation.tick(timestamp);
        })
    }
}

export default new Animator();
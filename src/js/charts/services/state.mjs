import EventManager from './eventManager.mjs';
import chartsData from '../../../data/charts.mjs';
import themesData from '../../../data/themes.mjs';

export default {
    isAnimating: true,
    isDebugMode: false,
    lastRenderTimestamp: null,

    data: {
        charts: chartsData,
        themes: themesData,
    },

    renderer: null,
    viewport: {
        width: 0,
        height: 0,
    },
    debug: {
        fps: 0,
    },

    charts : new Map(),
    selection: {
        min: 0.8,
        max: 1,
        scale: 1,
    },

    activeChart: null,
    activeLines: [],

    activeTheme: null,

    set animate(flag) {
        flag = flag || false;
        if(this.isAnimating === flag) return;
        this.isAnimating = flag;
        EventManager.dispatchEvent('isAnimatingStateChanged');
    },
    set debugMode(state) {
        this.isDebugMode = state || false;
        EventManager.dispatchEvent('isDebugModeStateChanged');
    },

    clearCharts() {
        this.charts = new Map();
        this.activeChart = null;
        this.activeLines = [];
    },
    addChart(chart) {
        this.charts.set(this.charts.size+1, chart);
    },
};
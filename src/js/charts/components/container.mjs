import EventManager from '../services/eventManager.mjs';
import state from '../services/state.mjs';
import { Point } from './point.mjs';

class Container {
    sortOrder = null;
    isActive = true;
    isFirstRender = true;
    dirty = true;
    parent = null;
    canvas = null;
    renderer = null;
    children = new Map();
    childrenSortOrder = [];


    size = {
        type: 'relative', //relative, fixed
        relation: {
            width: 100,
            height: 100,
        },
        width: 0,
        height: 0,
    };
    position = new Point();
    scale = new Point(1,1);
    opacity = 1;

    previous = {
        size: null,
    };
    data = new Map();

    constructor() {
        this.initCanvas();
        this.initRenderer();
        this.initEvents();
    }

    initCanvas() {
        if (typeof OffscreenCanvas == 'function') {
            this.canvas = new OffscreenCanvas(this.size.width, this.size.height);
        } else {
            this.canvas = document.createElement('canvas');
        }
    }

    updateCanvasSize() {
        if(!this.canvas) return;
        this.canvas.width = this.size.width;
        this.canvas.height = this.size.height;
    }

    updateSize() {
        //copy size from parent if not set
        if(this.size.type == 'relative' && this.parent && this.parent.size) {
            if(this.size.relation.width) this.size.width = Math.ceil((this.parent.size.width/100) * this.size.relation.width);
            if(this.size.relation.height) this.size.height =  Math.ceil((this.parent.size.height/100) * this.size.relation.height);
        }

        //updating canvas width/height will clear it, so we need to redraw current container
        if(!this.previous.size || (this.previous.size.width != this.size.width || this.previous.size.height != this.size.height)) {
//console.log('updateSize', this.constructor.name, (this.previous.size) ? this.previous.size.width || false : null, this.size.width, (this.previous.size) ? this.previous.size.height || false : null, this.size.height);
            this.updateCanvasSize();
            this.isDirty = true;

            this.previous.size = Object.assign({}, this.size);
        }

        this.children.forEach((container, key) => {
            container.updateSize();
        });


    }

    initRenderer() {
        this.renderer = this.canvas.getContext('2d');
        this.renderer.imageSmoothingEnabled = false;
    }

    initEvents() {}

    get positionGlobal() {
        const result = new Point(this.position.x, this.position.y);
        if(this.parent && this.parent.positionGlobal) {
            result.x += this.parent.positionGlobal.x;
            result.y += this.parent.positionGlobal.y;
        }
        return result;
    }

    get isDirty() {
        return (this.isActive && this.dirty);
    }

    set isDirty(value) {
        this.dirty = value;

        //override if there are still dirty children
        if(!this.dirty && this.hasDirtyChildren()) {
            this.dirty = true;
        }

        //contaminate parents if dirty
        if(this.dirty && this.parent) {
            this.parent.isDirty = true;
        }
    }

    hasDirtyChildren() {
        if(!this.children.size) return false;
        let result = false;
        this.children.forEach((child, key) => {
            if(child.isDirty) result = true;
        });
        return result;
    }

    addChild (name, container) {
        container.parent = this;
        container.updateSize();

        this.children.set(name, container);
    }
    getChild(name) {
        return this.children.get(name) || null;
    }

    updateChildrenOrder() {
        this.childrenSortOrder = [];

        const children = [];
        this.children.forEach((child, name) => {
            children.push({name: name, sortOrder: child.sortOrder || 1});
        });
        children.sort((a, b) => {
            return a.sortOrder > b.sortOrder ? 1 : ( a.sortOrder < b.sortOrder ? -1 : 0 );
        });

        for(let i in children) {
            let child = children[i];
            this.childrenSortOrder.push(child.name);
        }
// console.log('updateChildrenOrder', this.constructor.name, this.childrenSortOrder);
    }

    draw() {
        if(!state.renderer) return;

        //just don't process opaque items, instead of drawing them
        if(this.isActive && this.isDirty && !this.opacity) {
            this.isActive = false;
        }

        if(this.isActive && this.isDirty) {
//let time = Date.now();
            this.renderPreProcess();
            this.render();
            this.renderPostProcess();
// if(this.parent && this.parent.parent && this.constructor.name != 'Debug') {
//let timeDiff = Date.now() - time;
// if(timeDiff > 2) {
//     console.log('draw time', this.constructor.name+' -> '+this.parent.constructor.name, timeDiff);
// }
// }

        }

        if(!this.isActive || !this.opacity) return;
        if(this.parent && this.size.width && this.size.height) {
// console.log('drawImage', this.constructor.name+' -> '+this.parent.constructor.name, this.renderer.globalAlpha);
            this.parent.renderer.drawImage(this.canvas, this.position.x, this.position.y, this.size.width * this.scale.x, this.size.height * this.scale.y);
        }
    }

    clear() {
// console.log('clear', this.constructor.name);
        this.renderer.moveTo(0, 0);
        this.renderer.clearRect(0, 0, this.size.width, this.size.height);
    }

    renderPreProcess() {
        if(this.isFirstRender) {
            this.isFirstRender = false
        } else {
            this.clear();
        }

        //update sort order if amount of children changed
        if(this.children.size && this.children.size != this.childrenSortOrder.length){
            this.updateChildrenOrder();
        }

        this.renderer.globalAlpha = this.opacity;
    }

    render() {
// console.log('render', this.constructor.name, this.opacity, this.renderer.globalAlpha);
        this.renderChildren();
    }

    renderChildren() {
// if(this.constructor.name == 'Box') console.log('renderChildren', this.constructor.name, this.opacity);
        if(!this.children.size) return;
        this.childrenSortOrder.forEach((name, index) => {
            const child = this.children.get(name);
            child.draw();
        });
    }

    renderPostProcess() {
        this.isDirty = false;
    }
}

export { Container };
import {Point} from "./point.mjs";

class Rect {

    constructor(position, size) {
        this.position = position || new Point(0, 0);
        this.size = size || {width: 0, height: 0};
    }


    contains(point) {
// console.log(point.x, point.y);
// console.log(this.position.x, this.position.y, this.position.x + this.size.width, this.position.y + this.size.height);
        return point.x >= this.position.x &&
            point.x <= this.position.x + this.size.width &&
            point.y >= this.position.y &&
            point.y <= this.position.y + this.size.height;
    }

}

export { Rect };
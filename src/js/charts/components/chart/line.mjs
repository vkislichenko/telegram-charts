import {Point} from "../point.mjs";
import aggregator from "../../services/aggregator.mjs";

class Line {
    chart = null;
    points = [];
    vPoints = null;
    uid = null;
    name = null;
    color = null;
    bounds = {
        min: new Point(),
        max: new Point()
    };
    visibleBounds = {
        min: new Point(),
        max: new Point()
    };
    isActive = true;

    set isDirty(value) {
        if(!value) return;
        this.vPoints = null;
    }

    get visiblePoints() {
        if(this.vPoints) return this.vPoints;

        //filter out invisible points
        const points = this.points;
        const bounds = aggregator.visibleBoundsAxisX;
        const result = points.filter((point, pos, arr) => {
            return (point.x >= bounds.min && point.x <= bounds.max);
        });

        this.vPoints = result;
        this.updateVisibleBounds();

        return this.vPoints;
    }

    updateBounds() {
        //x axis
        const firstPoint = this.points[0];
        const lastPoint = this.points[this.points.length - 1];
        this.bounds.min.x = firstPoint.x;
        this.bounds.max.x = lastPoint.x;

        //y axis
        this.bounds.min.y = 0;
        this.bounds.max.y = 0;
        this.points.forEach((point, index) => {
            if(point.y > this.bounds.max.y) this.bounds.max.y = point.y;
            if(point.y < this.bounds.min.y) this.bounds.min.y = point.y;
        });
    }

    updateVisibleBounds() {
        const visiblePoints = this.visiblePoints;
        if(!visiblePoints.length) {
            this.visibleBounds.min.x = -1;
            this.visibleBounds.max.x = -1;
            this.visibleBounds.min.y = -1;
            this.visibleBounds.max.y = -1;
            return;
        }

        //x axis
        const firstPoint = visiblePoints[0];
        const lastPoint = visiblePoints[this.visiblePoints.length - 1];
        this.visibleBounds.min.x = firstPoint.x;
        this.visibleBounds.max.x = lastPoint.x;

        //y axis
        this.visibleBounds.min.y = firstPoint.y;
        this.visibleBounds.max.y = lastPoint.y;
        visiblePoints.forEach((point, index) => {
            if(point.y > this.visibleBounds.max.y) this.visibleBounds.max.y = point.y;
            if(point.y < this.visibleBounds.min.y) this.visibleBounds.min.y = point.y;
        });
    }

}

export { Line };
import {Point as BasePoint}  from '../point.mjs';
class Point extends BasePoint{
    line = null;
    chart = null;
}

export { Point };
import state from "../../services/state.mjs";
import aggregator from "../../services/aggregator.mjs";
import EventManager from "../../services/eventManager.mjs";

class Charts {
    dom = {
        container: null,
        wrapper: null,
        select: null,
    };
    isActive = false;
    constructor(options) {
        this.dom.container = options.container;
        this.initWrapper();
        this.initEvents();
    }

    initEvents() {
        EventManager.addEventListener('afterActiveThemeSelected', this.afterActiveThemeSelected.bind(this));
    }

    initWrapper() {
        const domElement = document.createElement("div");
        domElement.setAttribute('class', 'charts-selector-wrapper');
        domElement.style.color = aggregator.theme.gui.fontColor;
        this.dom.container.appendChild(domElement);
        this.dom.wrapper = domElement;

        // const buttonDomElement = document.createElement("button");
        // buttonDomElement.innerHTML = 'Select Active Chart';
        // buttonDomElement.addEventListener('click', this.openChartsSelectionDialog.bind(this));
        // this.dom.wrapper.appendChild(buttonDomElement);

        const labelDomElement = document.createElement("label");
        this.dom.wrapper.appendChild(labelDomElement);

        const titleDomElement = document.createElement("span");
        titleDomElement.setAttribute('class', 'title');
        titleDomElement.innerHTML = 'Select Active Chart';
        labelDomElement.appendChild(titleDomElement);

        this.dom.select = document.createElement("select");
        this.dom.select.addEventListener('change', this.changeChart);
        labelDomElement.appendChild(this.dom.select);
    }

    afterActiveThemeSelected() {
        this.dom.wrapper.style.color = aggregator.theme.gui.fontColor;
    }

    afterChartDataImport() {
        //clear elements in select
        while (this.dom.select.firstChild) {
            this.dom.select.removeChild(this.dom.select.firstChild);
        }

        state.charts.forEach((chart, key) => {
            const domElement = document.createElement("option");
            domElement.setAttribute('value', key);
            domElement.innerText = 'Chart: #'+key;
            this.dom.select.appendChild(domElement);
        });
    }

    changeChart(event) {
        const domElement = event.currentTarget;
        const id = (domElement.options[domElement.selectedIndex].value) >> 0;

        const chart = state.charts.get(id);
        if(!chart) return;

        chart.activate();
    }

    openChartsSelectionDialog() {
        //EventManager.dispatchEvent('openChartsSelectionDialog');
    }
}

export {Charts};
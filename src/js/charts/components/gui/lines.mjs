import state from "../../services/state.mjs";
import aggregator from "../../services/aggregator.mjs";
import EventManager from "../../services/eventManager.mjs";

class Lines {
    dom = {
        container: null,
        wrapper: null,
        lines: {},
    };
    constructor(options) {
        this.dom.container = options.container;
        this.initWrapper();this.initEvents();
    }

    initEvents() {
        EventManager.addEventListener('afterActiveThemeSelected', this.afterActiveThemeSelected.bind(this));
    }

    initWrapper() {
        const domElement = document.createElement("div");
        domElement.setAttribute('class', 'lines-wrapper');
        this.dom.container.appendChild(domElement);
        this.dom.wrapper = domElement;
    }

    afterActiveThemeSelected() {
        for(let uid in this.dom.lines) {
            let domElement = this.dom.lines[uid];
            domElement.style.borderColor = aggregator.theme.gui.borderColor;
            domElement.style.color = aggregator.theme.fontColor;
        }
    }


    afterChartDataImport() {
        this.dom.lines = {};

        //clear elements in wrapper
        while (this.dom.wrapper.firstChild) {
            this.dom.wrapper.removeChild(this.dom.wrapper.firstChild);
        }
// console.log(state.activeLines);
        state.activeLines.forEach((line, key) => {
            const domElement = document.createElement("div");
            domElement.setAttribute('class', 'lines-selector');
            domElement.setAttribute('data-uid', line.name);
            domElement.checked = true;
            domElement.addEventListener('click', this.domCheckboxToggle);
            domElement.style.borderColor = aggregator.theme.gui.borderColor;
            domElement.style.color = aggregator.theme.fontColor;
            this.dom.wrapper.appendChild(domElement);
            this.dom.lines[line.uid] = domElement;

            const inputDomElement = document.createElement("span");
            inputDomElement.setAttribute('class', 'checkbox');
            inputDomElement.innerHTML = '&#10004;';
            inputDomElement.style.backgroundColor = line.color;
            domElement.appendChild(inputDomElement);

            const titleDomElement = document.createElement("span");
            titleDomElement.setAttribute('class', 'title');
            titleDomElement.innerHTML = 'Line: '+line.name;
            domElement.appendChild(titleDomElement);
        });
    }

    domCheckboxToggle(event) {
        const domElement = event.currentTarget;
        const uid = domElement.getAttribute('data-uid');
        const line = state.activeChart.lines.get(uid);
        if(!line) return;

        //pretend to act as a checkbox
        const checked = !domElement.checked;
        domElement.checked = checked;

        const inputDomElement = domElement.querySelector('.checkbox');
        inputDomElement.style.backgroundColor = (checked) ? line.color : '#ffffff';



        if(domElement.checked === false) {
            line.isActive = false;
            delete state.activeLines[uid];

            inputDomElement.style.borderColor = line.color;
            inputDomElement.style.backgroundColor = '#ffffff';
        } else {
            line.isActive = true;
            state.activeLines[uid] = line;

            inputDomElement.style.borderColor = 'transparent';
            inputDomElement.style.backgroundColor = line.color;
        }

        aggregator.clearData();
        EventManager.dispatchEvent('afterActiveLinesSelected');

//console.log(uid);

    }
}

export {Lines};
import state from "../../services/state.mjs";
import aggregator from "../../services/aggregator.mjs";
import EventManager from "../../services/eventManager.mjs";

class Theme {
    dom = {
        container: null,
        wrapper: null,
        button: null,
    };
    isActive = false;
    constructor(options) {
        this.dom.container = options.container;
        this.initWrapper();
    }

    initWrapper() {
        this.dom.wrapper = document.createElement("div");
        this.dom.wrapper.setAttribute('class', 'theme-selector-wrapper');
        this.dom.container.appendChild(this.dom.wrapper);

        this.dom.button = document.createElement("button");
        this.dom.button.style.color = aggregator.theme.gui.fontColor;
        this.dom.button.innerHTML = 'Switch to '+aggregator.nextTheme.name;
        this.dom.button.addEventListener('click', this.cycleSelectedThemes.bind(this));
        this.dom.wrapper.appendChild(this.dom.button);
    }

    cycleSelectedThemes(event) {
        aggregator.theme = aggregator.nextTheme;
        this.dom.button.style.color = aggregator.theme.gui.fontColor;
        this.dom.button.innerHTML = 'Switch to '+aggregator.nextTheme.name;
        EventManager.dispatchEvent('afterActiveThemeSelected');
    }

    openChartsSelectionDialog() {

    }
}

export {Theme};
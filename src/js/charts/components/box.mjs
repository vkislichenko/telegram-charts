import { Container } from './container.mjs';

class Box extends Container{
    text = '';
    font = {
        size: 16,
        family: 'serif',
        color: 'rgb(255, 255, 255)',
    };
    background = {
        color: 'rgb(0, 0, 0)',
    };
    border = {
        color: 'rgb(255, 255, 0)',
        width: 1,
    };
    padding = 5;

    constructor() {
        super();
        this.size.type = 'fixed'
    }

    render() {
        const fontSize = 16;
        this.renderer.moveTo(0, 0);

        if(this.background) {
            this.renderer.fillStyle = this.background.color;
            this.renderer.fillRect(0, 0, this.size.width, this.size.height);
        }

        if(this.border) {
            this.renderer.strokeStyle = this.border.color;
            this.renderer.lineWidth = this.border.width;
            this.renderer.strokeRect(0, 0, this.size.width, this.size.height);
        }


        this.renderer.fillStyle = this.font.color;
        this.renderer.font = this.font.size+'px '+this.font.family;
        this.renderer.fillText(this.text, this.padding, fontSize + this.padding);

        super.render();
    }

    //TODO: remove
    //do nothing, keep it dirty :)
    // renderPostProcess() {}
}

export { Box };
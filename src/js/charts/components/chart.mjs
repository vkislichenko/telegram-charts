import {Line} from './chart/line.mjs';
import {Point} from './chart/point.mjs';
import state from '../services/state.mjs';
import utils from "../services/utils.mjs";
import EventManager from "../services/eventManager.mjs";
import aggregator from "../services/aggregator.mjs";

class Chart {
    data = null;
    isActive = false;
    lines = new Map();

    constructor() {

    }

    import(data) {
        this.data = data;
        this.importColumns();
        this.importColors();

    }

    importColumns() {
        if(!this.data.columns) return;

        let timelineData = null;
        this.data.columns.forEach((values, columnKey) => {
            let columnId = values.shift();
            let columnType = (columnId in this.data.types) ? this.data.types[columnId] : null;

            switch(columnType) {
                case 'x':
                    timelineData = values;
                    break;
                case 'line':
                    this.importLine(columnId, values);
                    break;
            }
        });

        if(timelineData) {
            this.updateLinesTimeline(timelineData);
        }

        //update bounds after we have all points set
        this.lines.forEach((line, lineId) => {
            line.updateBounds();
        });
    }

    importColors() {
        for (const lineId in this.data.colors) {
            let color = this.data.colors[lineId];
            let line = this.lines.get(lineId);
            if(!line) continue;
            line.color = color;
        }
    }

    importLine(columnId, values) {
        const line = new Line();
        line.uid = utils.uuid();
        line.name = columnId;
        line.chart = this;

        values.forEach((value, key) => {
            let point = new Point(0 , value);
            point.line = line;
            point.chart = this;
            line.points[key] = point;
        });
        this.lines.set(columnId, line);
    }

    updateLinesTimeline(timelineData) {
        this.lines.forEach((line, lineId) => {
            timelineData.forEach((value, key) => {
                let point = (key in line.points) ? line.points[key] : null;
                if(!point) return;
                point.x = value;
            });
        })
    }

    activate() {
        if(state.activeChart) {
            if(state.activeChart === this) return;
            state.activeChart.isActive = false;
            state.activeChart.lines.forEach((line, name) => {
                line.isActive = false;
            });
        }
        this.isActive = true;
        state.activeChart = this;
        state.activeLines = [];
        this.lines.forEach((line, lineId) => {
            line.isActive = true;
            state.activeLines.push(line);
        });
        aggregator.clearData();
        EventManager.dispatchEvent('afterActiveChartSelected');
    }
}

export { Chart };
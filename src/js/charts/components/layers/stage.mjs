import EventManager from "../../services/eventManager.mjs";
import state from "../../services/state.mjs";
import { Container } from '../container.mjs';

class Stage extends Container {
    initEvents() {
        super.initEvents();
        EventManager.addEventListener('resize', this.resize.bind(this));
    }

    resize() {
        this.size.width = state.viewport.width;
        this.size.height = state.viewport.height;

        super.updateSize();
    }
}
export { Stage };
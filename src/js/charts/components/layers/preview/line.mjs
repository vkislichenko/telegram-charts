import { Container } from '../../container.mjs';
import aggregator from "../../../services/aggregator.mjs";
import EventManager from "../../../services/eventManager.mjs";

class Line extends Container{

    line = null;

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterActiveChartSelected', this.updateSelection.bind(this));
    }

    updateSelection() {
        this.isDirty = true;
        this.line.isDirty = true;
    }

    renderPreProcess() {
        super.renderPreProcess();

        if(!this.line) return;
        this.isActive = this.line.isActive;
    }

    render() {
        // const firstPoint = this.points[0];
        // const lastPoint = this.points[this.points.length - 1];
        const bounds = aggregator.bounds;

        const scale = {
            x: ((bounds.max.x - bounds.min.x) / this.size.width),
            y: ((bounds.max.y - bounds.min.y) / this.size.height),
        };
// console.log(scale);
        //set line color
        this.renderer.strokeStyle = this.line.color;
        this.renderer.lineWidth = 2;

        //render all points
        this.renderer.beginPath();

        this.line.points.forEach((point, index) => {
            let x = ((point.x - bounds.min.x) / scale.x) >> 0;
            // let y = (this.size.height - point.y) >> 0;
            let y = (this.size.height - ((point.y - bounds.min.y) / scale.y)) >> 0;
// console.log(x, point.y)
            if(index == 0) {
                this.renderer.moveTo(x, y);
            } else {
                this.renderer.lineTo(x, y);
            }
        });
        this.renderer.stroke();

        super.render();
    }


}

export { Line };
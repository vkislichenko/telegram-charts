import { Container } from '../../container.mjs';
import EventManager from "../../../services/eventManager.mjs";
import PointerEventManager from "../../../services/pointerEventManager.mjs";
import {PointerHandler} from "../../../model/pointerHandler.mjs";
import state from '../../../services/state.mjs';
import aggregator from "../../../services/aggregator.mjs";


class Selector extends Container {
    sortOrder = 100;
    selector = {
        initialized: false,
        position: {
            x: -100,
            y: 0,
        },
        size: {
            width: 100,
            height : 0,
        },
        padding: {
            x: 12,
            y: 3,
        }
    };

    constructor() {
        super();
        this.initHitMap()
    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterActiveLinesSelected', this.updateSelector.bind(this));
        EventManager.addEventListener('afterActiveThemeSelected', this.afterActiveThemeSelected.bind(this));
    }

    afterActiveThemeSelected() {
        this.isDirty = true;
    }

    updateSize() {
        super.updateSize();
        this.updateSelector();
    }

    updateSelector() {
        if(!this.size.width) return;

        //init selector size & position
        if(!this.selector.initialized) {
            this.selector.size.height = this.size.height;
            this.selector.position.y = ((this.size.height/2) - this.selector.size.height / 2) >> 0;
            this.selector.position.x = (this.size.width - this.selector.size.width) >> 0;
            this.selector.initialized = true;
        }

        //move selector if viewport is small
        if((this.selector.position.x+this.selector.size.width) > this.size.width) {
            this.selector.position.x = (this.size.width - this.selector.size.width) >> 0;
            if(this.selector.position.x < 0) {
                this.selector.position.x = 0;
                this.selector.size.width = this.size.width;
            }
        }

        this.updateSelection();
        this.isDirty = true;
    }

    updateSelection() {
        state.selection.scale = this.selector.size.width / this.size.width;
        state.selection.min = this.selector.position.x / this.size.width;
        state.selection.max = (this.selector.position.x+this.selector.size.width) / this.size.width;

        aggregator.clearVisibleData();
        EventManager.dispatchEvent('afterPreviewSelectorUpdate');
    }

    initHitMap() {
        //selector left
        const selectorLeft = new PointerHandler();
        selectorLeft.events.set('update-rect', this.updateHitBoxSelectorLeft.bind(this));
        selectorLeft.events.set('drag', this.onSelectorLeftDrag.bind(this));
        PointerEventManager.hitMap.set('selector-left', selectorLeft);

        //selector center
        const selectorCenter = new PointerHandler();
        selectorCenter.events.set('update-rect', this.updateHitBoxSelectorCenter.bind(this));
        selectorCenter.events.set('drag', this.onSelectorCenterDrag.bind(this));
        PointerEventManager.hitMap.set('selector-center', selectorCenter);

        //selector right
        const selectorRight = new PointerHandler();
        selectorRight.events.set('update-rect', this.updateHitBoxSelectorRight.bind(this));
        selectorRight.events.set('drag', this.onSelectorRightDrag.bind(this));
        PointerEventManager.hitMap.set('selector-right', selectorRight);
    }

    updateHitMap() {
        const handlers = ['selector-left', 'selector-right', 'selector-center'];
        handlers.forEach((name, index) => {
            const handler = PointerEventManager.hitMap.get(name);
            if(!handler) return;
            const event = handler.events.get('update-rect');
            if(!event) return;
            event(handler.rect);
        });
    }

    onSelectorLeftDrag(point, dragStart) {
        let offsetX = (dragStart.x - point.x) >> 0;
        let result = true;

        //prevent overflow
        if((this.selector.position.x - offsetX) < 0) {
            offsetX = this.selector.position.x;
            result = false;
        }
        //prevent clipping
        if((this.selector.size.width + offsetX) < this.selector.padding.x*2) {
            offsetX = (this.selector.size.width - this.selector.padding.x*2) * -1;
            result = false;
        }

        this.selector.position.x -= offsetX;
        this.selector.size.width += offsetX;
        this.isDirty = true;

        this.updateSelection();

        return result;
    }
    onSelectorRightDrag(point, dragStart) {
        let offsetX = (dragStart.x - point.x) >> 0;
        let result = true;

        //prevent overflow
        if((this.selector.position.x + this.selector.size.width - offsetX) > this.size.width) {
            this.selector.position.x = this.size.width - this.selector.size.width
            offsetX = 0;
            result = false;
        }
        //prevent clipping
        if((this.selector.size.width - offsetX) < this.selector.padding.x*2) {
            this.selector.size.width = this.selector.padding.x*2;
            offsetX = 0;
            result = false;
        }

        this.selector.size.width -= offsetX;
        this.isDirty = true;

        this.updateSelection();

        return result;
    }
    onSelectorCenterDrag(point, dragStart) {
        let offsetX = (dragStart.x - point.x) >> 0;
        let result = true;

        //prevent overflow
        if((this.selector.position.x - offsetX) < 0) {
            offsetX = this.selector.position.x;
            result = false;
        }
        if((this.selector.position.x + this.selector.size.width - offsetX) > this.size.width) {
            this.selector.position.x = this.size.width - this.selector.size.width
            offsetX = 0;
            result = false;
        }

        this.selector.position.x -= offsetX;
        this.isDirty = true;

        this.updateSelection();

        return result;
    }

    updateHitBoxSelectorLeft(rect) {
// console.log('updateHitBoxSelectorLeft', this.positionGlobal);
        const offsetX = 20;
        rect.position.x = this.positionGlobal.x + (this.selector.position.x - offsetX);
        rect.position.y = this.positionGlobal.y + this.selector.position.y;
        rect.size.width = offsetX*2;
        rect.size.height = this.selector.size.height;
    }
    updateHitBoxSelectorRight(rect) {
        const offsetX = 20;
        rect.position.x = this.positionGlobal.x + (this.selector.position.x + this.selector.size.width - offsetX);
        rect.position.y = this.positionGlobal.y + this.selector.position.y;
        rect.size.width = offsetX*2;
        rect.size.height = this.selector.size.height;
    }
    updateHitBoxSelectorCenter(rect) {
        const offsetX = 20;
        rect.position.x = this.positionGlobal.x + (this.selector.position.x + offsetX);
        rect.position.y = this.positionGlobal.y + this.selector.position.y;
        rect.size.width = this.selector.size.width - offsetX*2;
        rect.size.height = this.selector.size.height;
// console.log('updateHitBoxSelectorCenter', rect);
    }

    render() {
        //render transparent layer
        this.renderer.save();
        this.renderer.globalAlpha = 0.5; // set global alpha
        this.renderer.fillStyle = aggregator.theme.grid.preview.color;
        this.renderer.fillRect(0, 0, this.size.width, this.size.height);
        this.renderer.restore();

        //render selector box
        this.renderer.save();
        this.renderer.globalAlpha = 0.8; // set global alpha
        this.renderer.fillStyle = aggregator.theme.grid.preview.selectorColor;
        this.renderer.fillRect(this.selector.position.x, this.selector.position.y, this.selector.size.width, this.selector.size.height);
        this.renderer.restore();

        //render selector cutout
        const selectorCutout = {
            position: {
                x: this.selector.position.x + this.selector.padding.x,
                y: this.selector.position.y + this.selector.padding.y,
            },
            size: {
                width: (this.selector.size.width - this.selector.padding.x*2) >> 0,
                height : (this.selector.size.height - this.selector.padding.y*2) >> 0,
            },
        }
        this.renderer.save();
        this.renderer.fillStyle = "#ffffff";
        this.renderer.globalCompositeOperation = "xor";
        this.renderer.fillRect(selectorCutout.position.x, selectorCutout.position.y, selectorCutout.size.width, selectorCutout.size.height);
        // this.renderer.clip();
        this.renderer.restore();


        super.render();
    }

    renderPostProcess () {
        super.renderPostProcess();
        this.updateHitMap();
    }
}

export { Selector };
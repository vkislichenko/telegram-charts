import EventManager from "../../services/eventManager.mjs";
import aggregator from "../../services/aggregator.mjs";
import animator from "../../services/animator.mjs";
import {Container} from '../container.mjs';
import {Line as GridLine} from './grid/line.mjs';
import {Vertical as VerticalRuler} from './grid/ruler/vertical.mjs';
import {Horizontal as HorizontalRuler} from './grid/ruler/horizontal.mjs';
import {CrossSection} from "./grid/crossSection.mjs";

class Grid extends Container {
    isActive = false;

    constructor() {
        super();

        this.size.height = 600;
        this.size.relation.height = 0;
        this.initChildren();
        this.initAnimations();
    }

    initAnimations() {
        animator.addAnimation('gridUpdateLines', {
            // fpsLimit: 30,
            // frameAmount: 10,
            frameLoop: true,
            animate: this.animationUpdateLines.bind(this),
        });
    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterActiveChartSelected', this.afterActiveChartSelected.bind(this));
        EventManager.addEventListener('afterActiveLinesSelected', this.afterActiveLinesSelected.bind(this));
    }

    initChildren() {
        this.addChild('horizontalRuler', new HorizontalRuler());
        this.addChild('verticalRuler', new VerticalRuler());
        this.addChild('crossSection', new CrossSection());
    }

    updateHorizontalRuler() {
        const ruler = this.getChild('horizontalRuler');
        if(!ruler) return;
        ruler.position.y = this.size.height - ruler.size.height;
    }

    updateVerticalRuler() {
        //to some stuff
    }

    updateHorizontalRulerMarks()
    {
        const ruler = this.getChild('horizontalRuler');
        if(!ruler) return;

        ruler.isDirty = true;

        const dateOptions = {month: 'short', day: 'numeric' };
        let points = aggregator.pointsAxisX;
// points = points.slice(0, 10);
        points = points.map((value, key) => {
            const date = new Date(value);
            return {
                timestamp: value,
                text: date.toLocaleDateString("en-US", dateOptions)
            }
        });

        ruler.importMarks(points);
    }

    updateVerticalRulerMarks()
    {
        const ruler = this.getChild('verticalRuler');
        if(!ruler) return;

        ruler.isDirty = true;

        const points = aggregator.rulerPointsAxisY;
        ruler.importMarks(points);
    }

    updateLines() {
        const lines = aggregator.lines;
//console.log('updateLines', this.constructor.name, lines);
        lines.forEach((line, index) => {
            const childName = 'line'+line.uid

            //add line
            let gridLine = this.getChild(childName);
            if(!gridLine) {
                gridLine = new GridLine();
                gridLine.line = line;
                this.addChild(childName, gridLine);
            }

            gridLine.isActive = line.isActive;
            gridLine.isDirty = true;
        });
    }


    updateSize() {
        this.updateHorizontalRuler();
        this.updateVerticalRuler();
        super.updateSize();
    }

    afterActiveChartSelected() {
        this.data = new Map();
        this.activate();
    }

    afterActiveLinesSelected() {
        this.activate();
    }

    activate() {
        this.isDirty = true;
        this.isActive = true;
        this.updateHorizontalRulerMarks();
        this.updateVerticalRulerMarks();
        this.updateLines();
    }

    animationUpdateLines(animation)
    {
        const lines = aggregator.lines;
        if(!lines.length) {
            animation.active = false;
            return;
        }

        const list = [];

        list.push(this.animationUpdateLinesScale());
        list.push(this.animationUpdateLinesOffset());

        const active = list.filter(item => item === true);
        if(!active.length) animation.active = false;
    }

    animationUpdateLinesScale(){
        const lines = aggregator.lines;
        if(!lines.length) return false;

        const visibleScaleY = this.data.get('visibleScaleY');
        let scaleY = this.data.get('scaleY');
        if(!scaleY || scaleY === visibleScaleY) {
            return false;
        }

        const incrementPerFrame = Math.abs(visibleScaleY / 10);
// console.log('animationUpdateLinesScale', incrementPerFrame);
        let currentDiff = visibleScaleY - scaleY;

        if(currentDiff > 0) {
            scaleY += incrementPerFrame;
            if(scaleY > visibleScaleY) scaleY = visibleScaleY;
        } else {
            scaleY -= incrementPerFrame;
            if(scaleY < visibleScaleY) scaleY = visibleScaleY;
        }


// console.log('animationUpdateLinesScale', visibleScaleY, scaleY,incrementPerFrame);

        this.data.set('scaleY', scaleY);

        lines.forEach((line, index) => {
            const childName = 'line'+line.uid
            let gridLine = this.getChild(childName);
            if(!gridLine) return;

            gridLine.isDirty = true;
        });
        return true;
    }

    animationUpdateLinesOffset()
    {
        const lines = aggregator.lines;
        if(!lines.length) return false;

        const virtualOffsetY = this.data.get('virtualOffsetY');
        const incrementPerFrame = Math.abs(virtualOffsetY/50);

        let offsetY = this.data.get('offsetY');

        if(!offsetY || offsetY === virtualOffsetY) {
            return false;
        }

        let currentDiff = virtualOffsetY - offsetY;

        if(currentDiff > 0) {
            offsetY += incrementPerFrame;
            if(offsetY > virtualOffsetY) offsetY = virtualOffsetY;
        } else {
            offsetY -= incrementPerFrame;
            if(offsetY < virtualOffsetY) offsetY = virtualOffsetY;
        }
        this.data.set('offsetY', offsetY);

        lines.forEach((line, index) => {
            const childName = 'line'+line.uid
            let gridLine = this.getChild(childName);
            if(!gridLine) return;

            gridLine.isDirty = true;
        });

        return true;
    }

}

export { Grid };
import EventManager from "../../services/eventManager.mjs";
import state from "../../services/state.mjs";
import { Box } from '../box.mjs';

class Debug extends Box {
    constructor() {
        super();

        this.isActive = state.isDebugMode;
        this.position.x = 20;
        this.position.y = 20;

        this.size.width = 100;
        this.size.height = 32;
    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('isDebugModeStateChanged', this.debugModeStateChanged.bind(this));
    }

    debugModeStateChanged() {
        this.isActive = state.isDebugMode;
    }

    renderPreProcess() {
        super.renderPreProcess();
        this.text = 'FPS: ' + state.debug.fps.toFixed(4);
    }

    //do nothing, keep it dirty :)
    renderPostProcess() {}
}

export { Debug };
import { Container } from '../../container.mjs';
import EventManager from "../../../services/eventManager.mjs";
import aggregator from "../../../services/aggregator.mjs";
import state from "../../../services/state.mjs";
import animator from "../../../services/animator.mjs";

class Line extends Container{

    line = null;

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterPreviewSelectorUpdate', this.updateSelection.bind(this));
        EventManager.addEventListener('afterActiveChartSelected', this.afterActiveChartSelected.bind(this));
    }

    afterActiveChartSelected() {
        this.isDirty = true;
        this.line.isDirty = true;
        this.isActive = this.line.isActive;
    }

    updateSelection() {
        this.isDirty = true;
        this.line.isDirty = true;
    }

    renderPreProcess() {
        super.renderPreProcess();

        if(!this.line) return;
        this.isActive = this.line.isActive;
    }

    render() {
        const points = this.line.visiblePoints;
        const bounds = aggregator.bounds;
        const visibleBounds = aggregator.visibleBounds;

        const verticalRulerHeight = 30;
        const visibleCanvasHeight = this.size.height - verticalRulerHeight;

        const visibleScale = {
            // x: ((visibleBounds.max.x - visibleBounds.min.x) / this.size.width),
            y: ((visibleBounds.max.y - bounds.min.y) / visibleCanvasHeight),
        };

        //virtual width: size of select area multiplied by visible width
        const virtualCanvasWidth = this.size.width * (1/state.selection.scale);
        //virtual height: amount of visible axisY points
        const virtualCanvasHeight = (visibleBounds.max.y - bounds.min.y);

        const scale = {
            x: ((bounds.max.x - bounds.min.x) / virtualCanvasWidth),
            y: (bounds.max.y / virtualCanvasHeight),
        };



        let scaleY = this.parent.data.get('scaleY');
        if(!scaleY) {
            scaleY = visibleScale.y;
            this.parent.data.set('scaleY', scaleY);
        }
        if(scaleY !== visibleScale.y) {
            this.parent.data.set('visibleScaleY', visibleScale.y);
            animator.getAnimation('gridUpdateLines').activate();
        }


        const visibleVirtualOffsetX = virtualCanvasWidth * state.selection.min;
        const virtualOffsetY = bounds.min.y / scaleY;

        let offsetY = this.parent.data.get('offsetY');
        if(!offsetY) {
            offsetY = virtualOffsetY;
            this.parent.data.set('offsetY', offsetY);
        }
        if(offsetY !== virtualOffsetY) {
            this.parent.data.set('virtualOffsetY', virtualOffsetY);
            animator.getAnimation('gridUpdateLines').activate();
// console.log('scale', scaleY,  visibleScale.y);
        }


        //set line color
        this.renderer.strokeStyle = this.line.color;
        this.renderer.lineWidth = 2;

        //render all points
        this.renderer.beginPath();
        points.forEach((point, index) => {
            let x = (((point.x - bounds.min.x) / scale.x) - visibleVirtualOffsetX) >> 0;
            // let y = (((virtualCanvasHeight - (point.y - bounds.min.y)) / scaleY) + virtualOffsetY) >> 0;

            /**
             * from the bottom of the visible canvas
             * draw a scaled point ignoring bounds offset,
             * with offset for lowest first on a chart
             */
            let y = ((visibleCanvasHeight - ((point.y - bounds.min.y) / scaleY)) + offsetY) >> 0;

            if(index == 0) {
                this.renderer.moveTo(x, y);
            } else {
                this.renderer.lineTo(x, y);
            }
        });
        this.renderer.stroke();

        super.render();
    }
}

export { Line };
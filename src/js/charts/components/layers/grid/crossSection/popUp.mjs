import {Container} from "../../../container.mjs";
import aggregator from "../../../../services/aggregator.mjs";
import EventManager from "../../../../services/eventManager.mjs";

class PopUp extends Container {
    isActive = false;

    singleLineInfoWidth = 100;

    constructor() {
        super();

        this.size.type = 'fixed';
        // this.size.width = 200;
        this.size.height = 140;
        this.position.y = 10;

    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterActiveThemeSelected', this.afterActiveThemeSelected.bind(this));
    }

    afterActiveThemeSelected() {
        this.isDirty = true;
    }

    render() {
        super.render();

        const selection = this.parent.data.get('selection');
        if(!selection) return;

        //popup background and border
        this.renderer.save();
        this.renderer.fillStyle = aggregator.theme.backgroundColor;
        this.renderer.strokeStyle = aggregator.theme.grid.borderColor;
        this.renderer.lineWidth = 2;
        this.renderRoundRect(0, 0, this.size.width, this.size.height, 20);
        this.renderer.restore();

        //popup title with date from x axis
        const dateOptions = {weekday:'short', month: 'short', day: 'numeric' };
        const date = new Date(selection.x);

        const titleElement = {
            text: date.toLocaleDateString("en-US", dateOptions),
            padding: 15,
            font: {
                color:  aggregator.theme.fontColor,
                size: 20,
                family: 'Arial',
            }
        };
        const offsetX = 15;

        this.renderer.save();
        this.renderer.fillStyle = titleElement.font.color;
        this.renderer.font = titleElement.font.size+'px '+titleElement.font.family;
        this.renderer.fillText(titleElement.text, titleElement.padding, titleElement.font.size + titleElement.padding);
        this.renderer.restore();

        //popup data about axisY points
        const offsetY = titleElement.font.size + titleElement.padding + 15;
        const lineInfoWidth = this.singleLineInfoWidth;
        selection.y.forEach((point, index) => {
            //calc y position on canvas
            const pointOffsetX = (index*lineInfoWidth) + offsetX;

            //value
            let valueElement = {
                text: point.value,
                padding: 10,
                font: {
                    color:  point.color,
                    size: 22,
                    family: 'Arial',
                }
            };

            this.renderer.save();
            this.renderer.fillStyle = valueElement.font.color;
            this.renderer.font = 'bold '+valueElement.font.size+'px '+valueElement.font.family;
            this.renderer.fillText(valueElement.text, pointOffsetX, offsetY + valueElement.font.size + valueElement.padding);
            this.renderer.restore();

            //name

            let nameElement = {
                text: point.name,
                padding: 10,
                font: {
                    color:  point.color,
                    size: 18,
                    family: 'Arial',
                }
            };

            this.renderer.save();
            this.renderer.fillStyle = nameElement.font.color;
            this.renderer.font = nameElement.font.size+'px '+nameElement.font.family;
            this.renderer.fillText(nameElement.text, pointOffsetX, offsetY + valueElement.font.size + valueElement.padding + nameElement.font.size + nameElement.padding);
            this.renderer.restore();

        })

    }

    renderRoundRect(x, y, w, h, radius)
    {
        const r = x + w;
        const b = y + h;
        this.renderer.beginPath();
        this.renderer.moveTo(x+radius, y);
        this.renderer.lineTo(r-radius, y);
        this.renderer.quadraticCurveTo(r, y, r, y+radius);
        this.renderer.lineTo(r, y+h-radius);
        this.renderer.quadraticCurveTo(r, b, r-radius, b);
        this.renderer.lineTo(x+radius, b);
        this.renderer.quadraticCurveTo(x, b, x, b-radius);
        this.renderer.lineTo(x, y+radius);
        this.renderer.quadraticCurveTo(x, y, x+radius, y);
        this.renderer.fill();
        this.renderer.stroke();
    }


    updateSelection() {
        const selection = this.parent.data.get('selection');
        const selectionCanvasX = this.parent.data.get('selectionCanvasX');

        const minWidth = 200;
        this.size.width = selection.y.length * this.singleLineInfoWidth;
        if(this.size.width < minWidth) this.size.width = minWidth;

        const outerWidth = this.size.width + 10;
        let popUpPositionLeft = (this.parent.size.width - selectionCanvasX < outerWidth);
        const canvasOffsetX = (popUpPositionLeft) ? outerWidth : -10;

        this.position.x = (selectionCanvasX - canvasOffsetX) >> 0;

        this.updateSize();
        this.isActive = true;
        this.isDirty = true;
    }
}

export { PopUp };
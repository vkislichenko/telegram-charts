import {Container} from "../../../container.mjs";
import aggregator from "../../../../services/aggregator.mjs";
import EventManager from "../../../../services/eventManager.mjs";

class Selector extends Container {
    isActive = false;

    constructor() {
        super();

        this.size.relation.width = false;
        this.size.width = 20;
    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterActiveThemeSelected', this.afterActiveThemeSelected.bind(this));
    }

    afterActiveThemeSelected() {
        this.isDirty = true;
    }

    render() {
        const horizontalRulerHeight = 30;

        this.renderer.save();

        this.renderer.strokeStyle = aggregator.theme.grid.borderColor;
        this.renderer.lineWidth = 2;

        const canvasX = (this.size.width/2) >> 0;

        this.renderer.beginPath();
        this.renderer.moveTo(canvasX, 0);
        this.renderer.lineTo(canvasX, this.size.height - horizontalRulerHeight);
        this.renderer.stroke();

        this.renderer.restore();

        this.renderCircles();

        super.render();
    }

    renderCircles() {
        const selection = this.parent.data.get('selection');
        if(!selection) return;

        const bounds = aggregator.bounds;

        const horizontalRulerHeight = 30;
        const visibleCanvasHeight = this.size.height - horizontalRulerHeight;

        let scaleY = this.parent.parent.data.get('scaleY');
        let offsetY = this.parent.parent.data.get('offsetY');

        const canvasX = (this.size.width/2) >> 0;

        selection.y.forEach((point, index) => {
            //calc y position on canvas
            let canvasY = ((visibleCanvasHeight - ((point.value - bounds.min.y) / scaleY)) + offsetY) >> 0;

            this.renderer.save();

            this.renderer.fillStyle = aggregator.theme.backgroundColor;
            this.renderer.strokeStyle = point.color;
            this.renderer.lineWidth = 2;

            this.renderer.beginPath();
            this.renderer.arc(canvasX, canvasY, 5, 0, 2 * Math.PI);
            this.renderer.fill();
            this.renderer.stroke();

            this.renderer.restore();
        })
    }

    updateSelection() {
        const selectionCanvasX = this.parent.data.get('selectionCanvasX');

        this.position.x = (selectionCanvasX - (this.size.width/2)) >> 0;
        this.isActive = true;
        this.isDirty = true;
    }
}

export { Selector };
import {Container} from '../../container.mjs';
import {Selector} from "./crossSection/selector.mjs";
import {PopUp} from "./crossSection/popUp.mjs";
import {PointerHandler} from "../../../model/pointerHandler.mjs";
import PointerEventManager from "../../../services/pointerEventManager.mjs";
import aggregator from "../../../services/aggregator.mjs";
import state from "../../../services/state.mjs";
import EventManager from "../../../services/eventManager.mjs";

class CrossSection extends Container {
    sortOrder = 200;

    constructor() {
        super();
        this.initChildren();
        this.initHitMap();
    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterPreviewSelectorUpdate', this.deactivateChildren.bind(this));
        EventManager.addEventListener('afterActiveChartSelected', this.deactivateChildren.bind(this));
    }

    updateSize() {
        super.updateSize();
        this.updateHitMap();
        this.deactivateChildren();
    }

    initChildren() {
        //selector: vertical line crossing grid
        this.addChild('selector', new Selector());
        //popUp: block with selected points data
        this.addChild('popUp', new PopUp());
    }

    deactivateChildren() {
        let activeChildren = 0;
        this.children.forEach((child, name) => {
            if(child.isActive) activeChildren++
            child.isActive = false;
        });
        if(activeChildren) this.isDirty = true;
    }


    initHitMap() {
        //selector left
        const handler = new PointerHandler();
        handler.events.set('update-rect', this.updateHitBox.bind(this));
        handler.events.set('drag', this.onSelectorHover.bind(this));
        handler.events.set('hover', this.onSelectorHover.bind(this));
        PointerEventManager.hitMap.set('cross-section-selector', handler);
    }

    updateHitMap() {
        const handlers = ['cross-section-selector'];
        handlers.forEach((name, index) => {
            const handler = PointerEventManager.hitMap.get(name);
            if(!handler) return;
            const event = handler.events.get('update-rect');
            if(!event) return;
            event(handler.rect);
        });
    }


    updateHitBox(rect) {
        rect.position.x = this.positionGlobal.x;
        rect.position.y = this.positionGlobal.y;
        rect.size.width = this.size.width;
        rect.size.height = this.size.height;
    }

    onSelectorHover(point) {
        const bounds = aggregator.bounds;
        const visibleBounds = aggregator.visibleBounds;
        const virtualCanvasWidth = this.parent.size.width * (1/state.selection.scale);
        const scaleX = ((bounds.max.x - bounds.min.x) / virtualCanvasWidth);

        let axisPointX = (visibleBounds.min.x + (point.x * scaleX));
        let closestAxisPointX = aggregator.getClosestPointAxisX(axisPointX);
// console.log(visibleBounds.max.x, visibleBounds.min.x, (new Date(axisPointX)).toISOString().split('T')[0], (new Date(closestAxisPointX)).toISOString().split('T')[0]);
        const visibleVirtualOffsetX = virtualCanvasWidth * state.selection.min;
        let canvasX = (((closestAxisPointX - bounds.min.x) / scaleX) - visibleVirtualOffsetX) >> 0;

        this.data.set('selection', aggregator.getInfoByAxisPointX(closestAxisPointX));
        this.data.set('selectionCanvasX', canvasX);

        this.children.forEach((child, name) => {
            child.updateSelection();
        });
    }
}

export { CrossSection };

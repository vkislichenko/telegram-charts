import {Container} from "../../../../../container.mjs";
import aggregator from "../../../../../../services/aggregator.mjs";
import EventManager from "../../../../../../services/eventManager.mjs";

class Line extends Container {
    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterActiveThemeSelected', this.afterActiveThemeSelected.bind(this));
    }

    render() {
        this.renderer.strokeStyle = aggregator.theme.grid.borderColor;
        this.renderer.lineWidth = 3;

        this.renderer.beginPath();
        this.renderer.moveTo(0, this.size.height);
        this.renderer.lineTo(this.size.width, this.size.height);
        this.renderer.stroke();

        super.render();
    }

    afterActiveThemeSelected() {
        this.isDirty = true;
    }
}

export { Line };
import {Box} from "../../../../../box.mjs";
import EventManager from "../../../../../../services/eventManager.mjs";
import aggregator from "../../../../../../services/aggregator.mjs";

class Text extends Box{
    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterActiveThemeSelected', this.afterActiveThemeSelected.bind(this));
    }

    constructor() {
        super();

        this.background = null;
        this.border = null;
        this.font.color = aggregator.theme.grid.fontColor;
    }

    afterActiveThemeSelected() {
        this.font.color = aggregator.theme.grid.fontColor;
        this.isDirty = true;
    }
}

export { Text };
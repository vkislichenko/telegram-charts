import {Text} from "./mark/text.mjs";
import {Line} from "./mark/line.mjs";
import {Container} from "../../../../container.mjs";

class Mark extends Container{
    constructor() {
        super();

        this.isActive = false;
        this.opacity = 0;
// this.opacity = 1;

        this.iniChildren();
    }

    iniChildren() {
        const text = new Text();
        text.size.width = 60;
        text.size.height = 30;
        this.addChild('text',text);

        const line = new Line();
        line.isActive = false;
        this.addChild('line',line);
    }

    updateSize() {
        const line = this.getChild('line');
        const text = this.getChild('text');

        line.size.width = this.parent.size.width - text.size.width;
        text.position.y = this.size.height - text.size.height;

        line.isActive = true;
        line.isDirty = true;
        super.updateSize();
    }
}

export { Mark };
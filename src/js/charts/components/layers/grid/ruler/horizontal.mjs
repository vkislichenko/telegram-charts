import {Container} from '../../../container.mjs';
import {Mark} from "./horizontal/mark.mjs";
import animator from "../../../../services/animator.mjs";
import utils from "../../../../services/utils.mjs";
import aggregator from "../../../../services/aggregator.mjs";
import state from "../../../../services/state.mjs";
import EventManager from "../../../../services/eventManager.mjs";


class Horizontal extends Container {

    childWidth = 60;

    constructor() {
        super();

        this.size.height = 30;
        this.size.relation.height = false;

        this.initAnimations();
    }

    //skip actual sort, as children are sorted by default
    updateChildrenOrder() {
        this.childrenSortOrder = Array.from(this.children.keys());
    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterPreviewSelectorUpdate', this.updateMarks.bind(this));
    }

    initAnimations() {
        animator.addAnimation('gridHorizontalRulerFadeMarks', {
            // fpsLimit: 30,
            frameAmount: 10,
            frameLoop: true,
            animate: this.animationFadeMarks.bind(this),
        });
    }

    updateSize() {
        this.updateMarks();
        super.updateSize();
    }

    importMarks(data)
    {
        //deactivate existing children
        this.children.forEach((child, name) => {
            child.isActive = false;
        });
        this.isDirty = true;

        data.forEach((item, key) => {
            let child = this.getChild(item.timestamp);
            if(!child) {
                child = new Mark();
                child.size.width = this.childWidth;
                child.size.height = this.size.height;
                this.addChild(item.timestamp, child);
            }
            child.text = item.text;
        });

        //initial position set
        this.updateMarks();
    }



    updateMarks()
    {
        this.updateMarksPosition();
        this.deactivateInvisibleMarks();
        this.updateMarksAnimations();
        animator.getAnimation('gridHorizontalRulerFadeMarks').activate();
    }

    updateMarksPosition() {
        const points = aggregator.pointsAxisX;
        if(!points.length) return;

        const bounds = {
            min: points[0],
            max: points[points.length-1],
        };
        const visiblePoints = aggregator.visiblePointsAxisX;

        const markOffsetX = (this.childWidth/2) >> 0;
        const virtualCanvasWidth = (this.size.width - markOffsetX) * (1/state.selection.scale);
        const virtualOffsetX = virtualCanvasWidth * state.selection.min;

        const scale = (bounds.max - bounds.min) / virtualCanvasWidth;

        visiblePoints.forEach((point, index) => {
            const mark = this.children.get(point);
            if(!mark) return;

            //calc x position on canvas
            const x = (((point - bounds.min) / scale) - virtualOffsetX - markOffsetX) >> 0;

            mark.position.x = x;
            mark.isActive = true;
            mark.isDirty = true;
        });
    }

    deactivateInvisibleMarks() {
        const points = aggregator.visiblePointsAxisX;
        if(!points.length) return;

        const childKeys = Array.from(this.children.keys());
        const invisiblePoints = childKeys.filter(x => !points.includes(x));

        invisiblePoints.forEach((point, index) => {
            const mark = this.children.get(point);
            if(!mark) return;

            mark.isActive = false;
        });
    }

    updateMarksAnimations() {
        const points = aggregator.pointsAxisX;
        if(!points.length) return;

        const virtualCanvasWidth = this.size.width * (1/state.selection.scale);
        let fadeInMarksAmount = (virtualCanvasWidth / this.childWidth) >> 0;
        if(fadeInMarksAmount > points.length) {
            fadeInMarksAmount = points.length;
        }

        let fadeInPoints = points;
        if(fadeInMarksAmount < points.length) {
            fadeInPoints = utils.distributedCopy(points, fadeInMarksAmount);
        }

        const visiblePoints = aggregator.visiblePointsAxisX;
        visiblePoints.forEach((point, index) => {
            const mark = this.children.get(point);
            if(!mark) return;
            mark.data.set('animation', (fadeInPoints.indexOf(point) !== -1) ? 'fadeIn' : 'fadeOut');
        });
    }

    animationFadeMarks(animation){
        const points = aggregator.visiblePointsAxisX;
        if(!points.length) {
            animation.active = false;
            return;
        }

        const frameAmount = animation.options.frameAmount;
        const opacityPerFrame = 1/frameAmount;

        let animatedMarks = 0;
        points.forEach((point, index) => {
            const mark = this.children.get(point);
            if(!mark || !mark.isActive) return;

            let markAnimation = mark.data.get('animation')
            if(!markAnimation) return;
            animatedMarks++;

            let opacity = mark.opacity;
            switch(markAnimation) {
                case 'fadeIn':
                    opacity += opacityPerFrame;
                    if(opacity >= 1) opacity = 1;
                    break;
                case 'fadeOut':
                    opacity -= opacityPerFrame;
                    if(opacity <= 0) opacity = 0;
                    break;
                default: break;
            }

            mark.opacity = opacity;
            mark.isDirty = true;

            //clear animation
            if(opacity == 0 || opacity == 1) {
                mark.data.delete('animation');
            }
        });

        //deactivate if there is nothing to animate
// console.log(animatedMarks);
        if(!animatedMarks) {
            animation.active = false;
        }
    }

}

export { Horizontal };
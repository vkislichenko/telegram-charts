import { Container } from '../../../container.mjs';
import EventManager from "../../../../services/eventManager.mjs";
import {Mark} from "./vertical/mark.mjs";
import animator from "../../../../services/animator.mjs";
import aggregator from "../../../../services/aggregator.mjs";
import state from "../../../../services/state.mjs";
import utils from "../../../../services/utils.mjs";


class Vertical extends Container{
    sortOrder = 1;
    childHeight = 60;

    constructor() {
        super();

        this.size.relation.height = false;
        this.initAnimations();
    }

    updateSize() {
        this.size.height = this.parent.size.height - 30;

        super.updateSize();
    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterPreviewSelectorUpdate', this.updateMarks.bind(this));
    }

    initAnimations() {
        animator.addAnimation('gridVerticalRulerFadeMarks', {
            // fpsLimit: 30,
            frameAmount: 10,
            frameLoop: true,
            animate: this.animationFadeMarks.bind(this),
        });
    }

    importMarks(data)
    {
        //deactivate existing children
        this.children.forEach((child, name) => {
            child.isActive = false;
        });
        this.isDirty = true;

        //import new children
        data.forEach((item, key) => {
            let child = this.getChild(item);
            if(!child) {
                child = new Mark();
                child.size.relation.height = false;
                child.size.height = this.childHeight;

                this.addChild(item, child);
            }
            child.getChild('text').text = item;
        });
        //initial position set
        this.updateMarks();
    }

    updateMarks()
    {
        this.updateMarksPosition();
        this.deactivateInvisibleMarks();
        this.updateMarksAnimations();
        animator.getAnimation('gridVerticalRulerFadeMarks').activate();
    }

    updateMarksPosition() {
        const points = aggregator.pointsAxisY;
        if(!points.length) return;

        const bounds = aggregator.bounds;
        const visibleBounds = aggregator.visibleBounds;
        const rulerPointsAxisY = aggregator.rulerPointsAxisY;
// return;
//         const markOffsetY = (this.childWidth/2) >> 0;

        const horizontalRulerHeight = 30;
        const visibleCanvasHeight = this.size.height - horizontalRulerHeight;

        const visibleScaleY = ((visibleBounds.max.y - bounds.min.y) / visibleCanvasHeight);

        let scaleY = this.parent.data.get('scaleY');
        if(!scaleY) scaleY = visibleScaleY;

        const virtualOffsetY = bounds.min.y / scaleY;
        let offsetY = this.parent.data.get('offsetY');
        if(!offsetY) offsetY = virtualOffsetY;

        rulerPointsAxisY.forEach((point, index) => {
            const mark = this.children.get(point);
            if(!mark) return;

            //calc y position on canvas
            let y = ((visibleCanvasHeight - ((point - bounds.min.y) / scaleY)) + offsetY) >> 0;
// console.log(visibleCanvasHeight, point, bounds.min.y, scaleY, offsetY);
// console.log(mark.position.x, y, mark.size);
            mark.position.y = y;
            mark.isActive = true;
            mark.isDirty = true;
        });
    }

    deactivateInvisibleMarks() {
        const points = aggregator.pointsAxisY;
        if(!points.length) return;

        const rulerPointsAxisY = aggregator.rulerPointsAxisY;
        const visibleBounds = aggregator.visibleBounds;

        rulerPointsAxisY.forEach((point, key) => {
            if(point >= visibleBounds.min.y && point <= visibleBounds.max.y) return;

            const mark = this.children.get(point);
            if(!mark) return;

            mark.isActive = false;
        });
    }

    updateMarksAnimations() {
        const points = aggregator.pointsAxisY;
        if(!points.length) return;

        const rulerPointsAxisY = aggregator.rulerPointsAxisY;

        const verticalRulerHeight = 30;
        const visibleCanvasHeight = this.size.height - verticalRulerHeight;

        let fadeInMarksAmount = (visibleCanvasHeight / this.childHeight) >> 0;
        if(fadeInMarksAmount > rulerPointsAxisY.length) {
            fadeInMarksAmount = rulerPointsAxisY.length;
        }

        let fadeInPoints = rulerPointsAxisY;
        if(fadeInMarksAmount < rulerPointsAxisY.length) {
            fadeInPoints = utils.distributedCopy(rulerPointsAxisY, fadeInMarksAmount);
        }

        rulerPointsAxisY.forEach((point, index) => {
            const mark = this.children.get(point);
            if(!mark) return;
            mark.isActive = true;
            mark.data.set('animation', (fadeInPoints.indexOf(point) !== -1) ? 'fadeIn' : 'fadeOut');
        });
    }

    animationFadeMarks(animation){
        const points = aggregator.pointsAxisY;
        if(!points.length) {
            animation.active = false;
            return;
        }

        const rulerPointsAxisY = aggregator.rulerPointsAxisY;

        const frameAmount = animation.options.frameAmount;
        const opacityPerFrame = 1/frameAmount;

        let animatedMarks = 0;
        rulerPointsAxisY.forEach((point, index) => {
            const mark = this.children.get(point);
            if(!mark || !mark.isActive) return;

            let markAnimation = mark.data.get('animation')
// console.log('animationFadeMarks', point, markAnimation);
            if(!markAnimation) return;
            animatedMarks++;

            let opacity = mark.opacity;
            switch(markAnimation) {
                case 'fadeIn':
                    opacity += opacityPerFrame;
                    if(opacity >= 1) opacity = 1;
                    break;
                case 'fadeOut':
                    opacity -= opacityPerFrame;
                    if(opacity <= 0) opacity = 0;
                    break;
                default: break;
            }

            mark.opacity = opacity;
            mark.isDirty = true;

            //clear animation
            if(opacity == 0 || opacity == 1) {
                mark.data.delete('animation');
            }
        });

        //deactivate if there is nothing to animate
// console.log(animatedMarks);
        if(!animatedMarks) {
            animation.active = false;
        }
    }
}

export { Vertical };
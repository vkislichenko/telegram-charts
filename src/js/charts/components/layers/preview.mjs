import EventManager from "../../services/eventManager.mjs";
import state from "../../services/state.mjs";
import aggregator from "../../services/aggregator.mjs";
import { Container } from '../container.mjs';
import {Line as PreviewLine} from "./preview/line.mjs";
import {Selector as PreviewSelector} from "./preview/selector.mjs";



class Preview extends Container {
    isActive = false;

    constructor() {
        super();

        this.position.y = 600;
        this.size.height = 60;
        this.size.relation.height = false;
        this.initChildren();
    }

    initEvents() {
        super.initEvents();
        EventManager.addEventListener('afterActiveChartSelected', this.activate.bind(this));
        EventManager.addEventListener('afterActiveLinesSelected', this.activate.bind(this));
    }

    initChildren() {
        this.initSelector();
    }

    initSelector() {
        const selector = new PreviewSelector();
        this.addChild('selector', selector);
    }

    updateSize() {
        super.updateSize();
        // this.position.y = ((state.viewport.height / 100) * 60) >> 0;

    }

    updateLines() {
        const lines = aggregator.lines;
        //console.log('updateLines', this.constructor.name, lines);
        lines.forEach((line, index) => {
            const childName = 'line'+line.uid

            //add line
            let previewLine = this.getChild(childName);
            if(!previewLine) {
                previewLine = new PreviewLine();
                previewLine.line = line;
                this.addChild(childName, previewLine);
            }

            previewLine.isActive = line.isActive;
            previewLine.isDirty = true;
        });
    }

    activate() {
//console.log('activate', this.constructor.name);
        this.isDirty = true;
        this.isActive = true;
        this.updateLines();
    }
}

export { Preview };
import * as Components from './components/index.mjs';
import state from './services/state.mjs';
import aggregator from "./services/aggregator.mjs";
import animator from "./services/animator.mjs";
import EventManager from "./services/eventManager.mjs";
import PointerEventManager from "./services/pointerEventManager.mjs";
import {Lines as GuiLines} from "./components/gui/lines.mjs";
import {Charts as GuiCharts} from "./components/gui/charts.mjs";
import {Theme as GuiTheme} from "./components/gui/theme.mjs";



class Charts {
    config = {
        domContainer: null,
        domElementClass: 'charts',
        fpsLimit: null,
    };
    dom = {
        container: null,
        canvas: null,
    };
    gui = {};
    renderer = null;
    stage = null;

    constructor(config) {
        if(!this.initConfig(config)) return false;
        this.initCanvas();
        this.initRenderer();
        this.initEvents();
        this.initStage();
        this.initGui();
    }

    initConfig(config) {
        this.config = Object.assign({}, this.config, config || {});
        if(!this.config.domContainer) {
            console.error('Charts element container is not set');
            return false;
        }

        this.dom.container = this.config.domContainer;
        return true;
    }

    initCanvas() {
        const canvasDomElement = document.createElement("canvas");
        canvasDomElement.setAttribute('class', this.config.domElementClass);
        canvasDomElement.width = this.dom.container.clientWidth;
        canvasDomElement.height = this.dom.container.clientHeight - 3;
        this.dom.container.appendChild(canvasDomElement);
        this.dom.canvas = canvasDomElement;

        this.updateViewport();
    }

    initRenderer() {
        this.renderer = this.dom.canvas.getContext('2d');
        this.renderer.imageSmoothingEnabled = false;
        state.renderer = this.renderer;
    }

    initEvents() {
        EventManager.addEventListener('resize', this.resize.bind(this));
        EventManager.addEventListener('isAnimatingStateChanged', this.draw.bind(this));
        EventManager.addEventListener('afterChartDataImport', this.afterChartDataImport.bind(this));
        EventManager.addEventListener('afterActiveChartSelected', this.afterActiveChartSelected.bind(this));
        EventManager.addEventListener('afterActiveThemeSelected', this.afterActiveThemeSelected.bind(this));

        PointerEventManager.dom.canvas = this.dom.canvas;
        PointerEventManager.initCanvasEvents();
    }

    initStage() {
        this.stage = new Components.Layers.Stage();
        this.stage.parent = this;
        this.stage.resize();

        this.initLayers();
        if(state.isAnimating) this.draw();
    }

    initLayers() {
        const layers = ['Debug','Grid','Preview'];
        layers.forEach(layerClass => {
            const layer = new Components.Layers[layerClass];
//console.log(layer);
            this.stage.addChild(layerClass.toLowerCase(), layer);
        });
    }

    initGui() {
        this.gui.lines = new GuiLines({
            container: this.dom.container
        });
        this.gui.charts = new GuiCharts({
            container: this.dom.container
        });
        this.gui.theme = new GuiTheme({
            container: this.dom.container
        });
    }

    afterChartDataImport() {
        this.gui.lines.afterChartDataImport();
        this.gui.charts.afterChartDataImport();
    }

    afterActiveChartSelected() {
        this.gui.lines.afterChartDataImport();
    }

    afterActiveThemeSelected() {
        document.body.style.backgroundColor = aggregator.theme.backgroundColor;
    }

    updateViewport() {
        state.viewport.height = this.dom.canvas.clientHeight;
        state.viewport.width = this.dom.canvas.clientWidth;
    }

    resize() {
        this.dom.canvas.width = this.dom.container.clientWidth;
        this.dom.canvas.height = this.dom.container.clientHeight - 3;
        this.updateViewport();
        this.stage.isDirty = true;
    }

    clear() {
        this.renderer.moveTo(0, 0);
        this.renderer.clearRect(0, 0, state.viewport.width, state.viewport.height);
    }

    draw() {
        window.requestAnimationFrame(this.render.bind(this));
    }

    render(timestamp) {
        if(!state.lastRenderTimestamp) state.lastRenderTimestamp = timestamp;
        const delta = timestamp - state.lastRenderTimestamp;

        if(this.config.fpsLimit) {
            const deltaLimit = 1000/this.config.fpsLimit;
            if(delta && deltaLimit >= delta) {
                this.draw();
                return;
            }
        }

        if(state.isDebugMode && delta) {
            state.debug.fps = 1000/delta;
        }

        if(state.isAnimating) {
            animator.animate(timestamp);
        }

        if(this.stage.isDirty) {
            this.clear();
            this.stage.draw();
        }

        state.lastRenderTimestamp = timestamp;
        if(state.isAnimating) {
            this.draw();
        }
    }
}

export { Charts };
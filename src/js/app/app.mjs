import { Charts } from '../charts/charts.mjs';
import state from "../charts/services/state.mjs";
import aggregator from "../charts/services/aggregator.mjs";
import utils from "../charts/services/utils.mjs";
import EventManager from "../charts/services/eventManager.mjs";


class App {
    config = {
        domContainerElementId : 'charts-container',
        chartDataUri: 'data/chart_data.json',
    };

    constructor(config) {
        this.state = state; //for debug
        this.initConfig(config);
        this.initTheme();
        this.initDomElement();
        this.initCharts();
        this.loadChartsData();
    }

    initConfig(config) {
        this.config = Object.assign({}, this.config, config || {});
    }

    initTheme() {
        state.activeTheme = aggregator.themes.day;
    }

    initDomElement() {
        const domElement = document.createElement("div");
        domElement.setAttribute('id', this.config.domContainerElementId);
        document.body.appendChild(domElement);
        this.domContainerElement = domElement;
    }

    initCharts() {
        this.charts = new Charts({
            domContainer: this.domContainerElement,
            // fpsLimit: 1,
        });
    }

    loadChartsData() {
        //if data is loaded import it
        if(state.data.charts) {
            aggregator.importChartData(state.data.charts);
            return;
        }

        //if data is not already loaded load it via ajax
        fetch(this.config.chartDataUri)
            .then(function(response) {
                return response.json();
            })
            .then(function(chartData) {
                aggregator.importChartData(chartData);
            });
    }

    updateViewPort() {
        const isPortrait = window.matchMedia("(orientation: portrait)").matches;
// alert(isPortrait ? 'Portrait' : 'Landscape');
        let viewportWidth = isPortrait ? 600 : 1600;
        document.getElementById("viewport").setAttribute("content","width="+ viewportWidth +", user-scalable=no");
    }
}

export {App};



import {App} from "./js/app/app.mjs"
import EventManager from "./js/charts/services/eventManager.mjs";
import utils from "./js/charts/services/utils.mjs";


document.addEventListener("DOMContentLoaded", function(event) {
    const app = new App();
    window.app = app;

    window.app.updateViewPort();
    window.addEventListener('orientationchange', utils.debounce((event) => {
        window.app.updateViewPort();
    }, 500));
});


// window.addEventListener('resize', utils.debounce((event) => {
//     window.app.charts.updateViewport();
//     EventManager.dispatchEvent('resize');
// }, 1));

window.addEventListener('resize', (event) => {
    window.app.charts.updateViewport();
    EventManager.dispatchEvent('resize');
});
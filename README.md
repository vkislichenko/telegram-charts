#ES6 Charts App
Developed for Telegram official coding competition of 2019.03.11 - 2019.03.25

Current code is written without additional libraries, using native ES6 support of latest browser.
For production code is transpiled with [babel](https://babeljs.io/) to ES5 and bundled with [rollup](https://rollupjs.org/guide/en).  

#Http server
Use [http-server](https://www.npmjs.com/package/http-server) to serve index.html
### Native ES6
```bash
npm run start:dev
```
### Bundled
```bash
npm run start:prod
```

#Bundling
Babel and Rollup
```bash
npm run build
``` 
import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import multiEntry from 'rollup-plugin-multi-entry';
import uglifyPlugin from 'rollup-plugin-uglify';
import filesize from 'rollup-plugin-filesize';
import commonjs from 'rollup-plugin-commonjs';
import progress from 'rollup-plugin-progress';
import copy from 'rollup-plugin-copy-glob';

let pluginOptions = [
    multiEntry(),
    resolve({
        jsnext: true,
        browser: true
    }),
    commonjs(),
    progress(),
    babel({
        exclude: 'node_modules/**',
    }),
    uglifyPlugin.uglify(),
    filesize({
        showGzippedSize: false,
    }),
    copy([
        { files: 'src/**/*.{html,ico,json,css}', dest: 'build' },
        // { files: 'src/data/*.{json}', dest: 'build/data' },
        // { files: 'src/css/*.{css}', dest: 'build/css' },
        // { files: 'src/css/utils/*.{css}', dest: 'build/css/utils' },
    ], { verbose: true, watch: false }),
];

export default [{
    input: './src/index.mjs',
    output: {
        name: 'main',   // for external calls (need exports)
        file: 'build/bundle.js',
        format: 'umd',
    },
    plugins: pluginOptions,
}];